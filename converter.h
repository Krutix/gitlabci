#ifndef CONVERTER_H
# define CONVERTER_H

#include <string>
#include <exception>

float metric_conversion(float value, std::string const& from, std::string const& to);

#endif