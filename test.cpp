#define FTST_MAIN_FILE
#include "ftst.h"
#include "converter.h"

TEST(simple_test)
{
    FTST_EQ_A(metric_conversion(12, "mm", "sm"), 1.2, 0.0000001, f);
    FTST_EQ_A(metric_conversion(12, "km", "mm"), 12000000, 0.0000001, f);
    FTST_EQ_A(metric_conversion(12, "km", "sm"), 1200000, 0.0000001, f);
    FTST_EQ_A(metric_conversion(5, "m", "sm"), 500, 0.0000001, f);
    FTST_EQ_A(metric_conversion(5, "sm", "m"), 0.05, 0.0000001, f);
    FTST_EQ_A(metric_conversion(5, "km", "sm"), 500000, 0.0000001, f);
    FTST_EQ_A(metric_conversion(5, "sm", "km"), 0.00005, 0.0000001, f);
}

int main()
{
    FTST_INIT(stderr, "test_stat");
    FTST_RUNTEST(simple_test);
    return FTST_EXIT();
}