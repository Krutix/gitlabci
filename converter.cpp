#include "converter.h"

namespace
{
  uint32_t unit_value_to_mm(std::string const& v)
  {
    if (v == "mm")
      return 1;
    if (v == "sm")
      return 10;
    if (v == "dm")
      return 100;
    if (v == "m")
      return 1'000;
    if (v == "km")
      return 1'000'000;
    throw std::exception();
  }
}

float metric_conversion(float value, std::string const& from, std::string const& to)
{
  float mm_value = value * unit_value_to_mm(from);
  return mm_value / unit_value_to_mm(to);
}
