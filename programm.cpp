#include <iostream>
#include "converter.h"

int main()
{
  while (true)
  {
    float convert_value;
    std::string convert_from, convert_to;
    std::cin >> convert_value >> convert_from >> convert_to;
    try
    {
      std::cout << metric_conversion(convert_value, convert_from, convert_to) << '\n';
    }
    catch (std::exception const& e)
    {
      std::cout << e.what() << '\n';
    }
  }
}
